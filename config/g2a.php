<?php
return [
	'base_uri' 			=> 'https://www.g2a.com/api/v1/',
	'headers' 			=> [
		// 'Authorization' 	=> 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQzODhmNWY4ZWQ5NDgxZTYxZTg4NjViYjhiOTQ3OTY3MTlkNWU3MmI1MjI2MjNhYzg0ZGRjZmM3MjEwZWIxY2E4NDJjNDZlZmI5YTBmMTVhIn0.eyJhdWQiOiJnMmFfbW9iaWxlIiwianRpIjoiNDM4OGY1ZjhlZDk0ODFlNjFlODg2NWJiOGI5NDc5NjcxOWQ1ZTcyYjUyMjYyM2FjODRkZGNmYzcyMTBlYjFjYTg0MmM0NmVmYjlhMGYxNWEiLCJpYXQiOjE0NzU4MzcwMjAsIm5iZiI6MTQ3NTgzNzAyMCwiZXhwIjoxNDc1ODQwNjIwLCJzdWIiOiI2NWU2Mzg5Zi0zOThhLTQyNzAtYmEzZS1lMTA2NjJkY2RjMjciLCJzY29wZXMiOltdfQ.jIaMjdAu-HuApzr27hzhoWTzI1S57fFPtgxRXYXBQbqpU11mN97saxEWJxGopJq8h2grAVvLTa6FVDJ4DgxT6JWgvjeb1X-HyohbVNGje1zrrlaCeOtpl27xF7Svun6qUOzwtsiiArJyreJlQjco-T2F80DCumX8hDYihdhxXyIFGlRY67E-oE3QBjB1aSZVI2fR8fWjG9NOqgpxgp5AKCNI9lAzlzZ4WYeb9d-hCuegluCMiNr7-w7YEfWZUQrDZBcAY__tiMfG-DFSPG3jcqJ2dwMhUxGiqpmwjesxtUB2tOHi6eC7xzKPMDn5_ArReTmfIlo__9DMviXKM_v1PGCaY7q_vqgEr1Utg5MVVHPmLkrZBfhkGoMkZUUB1t3FBP01ik09DyL-MeouYdDys97Y-zaFDxYhEr_zNHtWi12M1OurG29bbE5XXVVmjvhYqOHWojZ77w6Y-pqNPXGb28p3CckGtvaPBjDFL2G5Fb78rzt_fpgQzz6ur9Y9fSxhmhk2YUGtjSK379LF7A04dVWFVaZupSWaI2gAxfoYIP_c4UFU8GdhHEma6jTgvnqZ0HdAvyWkxJkFVT--C527-jX8f1zK4iTo_MzFug5Ju0__uFtGKfYyojTU9Jrh-VEbEoSuG0hklL65ZFgTT1e9eXXfyBkZaxmi2PZER2FyjpM',
		'Content-Type' 		=> 'application/json',
		'User-Language' 	=> 'US',
		'User-Currency' 	=> 'EUR',
		'App-Version' 		=> '119',
		'Host' 				=> 'www.g2a.com',
		// 'Connection' 		=> 'Keep-Alive',
		// 'Accept-Encoding' 	=> 'gzip',
		'User-Agent' 		=> 'okhttp/3.4.1',
	],
];
