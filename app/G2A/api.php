<?php
namespace App\G2A;

use GuzzleHttp\Client;

class API {

	public function getProductById($id) {
		$query = [
			'id' => $id,
		];

		$response = $this->callAPI('product/', $query);
		return $response;
	}

	public function getAuctionsByProductId($product_id) {
		$query = [
			'id' 		=> $product_id,
			'page' 		=> 1,
			'pageSize' 	=> 50,
		];

		$response = $this->callAPI('product/auctions/', $query);
		return $response->data;
	}

	private function callAPI($endpoint, $query = []) {
		$client 	= new Client([
			'base_uri' 	=> config('g2a.base_uri'),
			'headers' 	=> config('g2a.headers', []),
			'verify' 	=> false,
		]);
		$res = $client->request('GET', $endpoint, ['query' => $query]);
		return json_decode($res->getBody());
	}

}
