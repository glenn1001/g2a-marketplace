<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Product;

class GetG2AProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:g2a:get:product
                            {entity_id : The entity ID of the product}
                            {--tracked=0 : Whether the product should be tracked}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get and instert or update the G2A product.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $api = new \App\G2A\API();
        $entity = $api->getProductById($this->argument('entity_id'));
        $product = Product::where('entity_id', $entity->entity_id)->first();
        if ($product === null) {
            $product = new Product();
            $product->entity_id = $entity->entity_id;
        }
        $product->title     = $entity->name;
        $product->url       = $entity->url_path;
        $product->image     = $entity->media_gallery->images[0]->thumbnail;
        $product->tracked   = $this->option('tracked') == "0" ? 0 : 1;
        $product->save();
    }
}
