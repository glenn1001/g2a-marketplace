<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Product;

class UpdateG2AProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:g2a:update:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all G2A products.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Product::all();
        foreach ($products as $product) {
            \Artisan::call('command:g2a:get:product', [
                'entity_id' => $product->entity_id, '--tracked' => $product->tracked
            ]);
        }
    }
}
