<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Product;
use App\Auction;

class GetG2AAuctions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:g2a:get:auctions
                            {--product_id= : The ID of the product}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get/update the auctions.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('product_id') === null) {
            $products = Product::where('tracked', 1)->get();
        } else {
            $products = Product::where('product_id', $this->option('product_id'))->get();
        }

        $api = new \App\G2A\API();
        foreach ($products as $product) {
            $entities = $api->getAuctionsByProductId($product->entity_id);
            foreach ($entities as $entity) {
                $auction = Auction::where('entity_id', $entity->id)->first();
                if ($auction === null) {
                    $auction = new Auction();
                    $auction->entity_id = $entity->id;
                }
                $auction->product_id    = $product->id;
                $auction->price         = $entity->price;
                $auction->currency      = $entity->currency;
                $auction->country       = $entity->country;
                $auction->rating        = $entity->rating;
                $auction->votes         = $entity->votes;
                $auction->save();
            }
        }
    }
}
